<?php
/*
 * 说明：加密类
**************************
update: 2014-5-31 21:58:30
person: xiaoyu
**************************
*/
/*文件根目录*/
define('ROOT',dirname(__FILE__));
class opensslClass
{
	private $priv_path = '/cert/priv_key.pfx';
	private $pub_path = '/cert/pub_key.cer';
	private $private_path = '/cert/priv_key.pem';
	private $pubilc_path = '/cert/pub_key.pem';
	public $_options = array('config' => 'D:\phpStudy\PHPTutorial\Apache\conf\openssl.cnf');
	public $dn = array(  
        		"countryName" => 'Zh', //所在国家名称  
		        "stateOrProvinceName" => 'Shanxi', //所在省份名称  
		        "localityName" => 'Xian', //所在城市名称  
		        "organizationName" => 'Rain',   //注册人姓名  
		        "organizationalUnitName" => 'Shaw', //组织名称  
		        "commonName" => 'xy', //公共名称  
		        "emailAddress" => '864727542@qq.com' //邮箱  
			);
	/*秘钥对配置*/
	public $cf = array(
				'config' => 'D:\phpStudy\PHPTutorial\Apache\conf\openssl.cnf',
				'digest_alg' => 'sha512',
				'private_key_bits' => 1024,//指定应使用多少位来生成私钥
				'private_key_type' => OPENSSL_KEYTYPE_RSA
			);
	private $key = '';  
	public function __construct()
	{
        if(!extension_loaded('openssl'))
        {
            \taskphp\Console::log('ERROR:openssl module has not been opened');die;
        }
        $this->key = md5('%$%$%$%$%$%$%$%');
    }
    /*生成新的私钥*/
    private function creatPkey()
    {
    	/*生成一个新的私钥*/
    	$pkey = openssl_pkey_new($this->cf);
    	return $pkey;
    }
    /*生成证书*/
    public function creatOpenssl()
    {
    	/*生成一个新的私钥*/
    	$res = $this->creatPkey();
    	/*生成一个 CSR*/
    	$csr = openssl_csr_new($this->dn,$res,$this->_options);
    	/*从给定的 CSR 生成一个x509证书资源*/
    	$x509 = openssl_csr_sign($csr,null,$res,365,$this->_options);
    	$csrkey = openssl_x509_export_to_file($x509,ROOT.$this->pub_path); //导出证书$csrkey 
    	/*输出一个 PKCS#12 兼容的证书存储文件*/
    	$csrkey = openssl_pkcs12_export_to_file($x509,ROOT.$this->priv_path,$res,'123456');
    	var_dump($csrkey);
    }
    /*生成秘钥对*/
    public function creatRas()
    {
    	/*生成一个新的私钥*/
    	$res = $this->creatPkey();
    	/*将私钥写入文档*/
    	openssl_pkey_export($res,$priv_key,$this->key,$this->_options);
    	/*获取公钥*/
    	$arr = openssl_pkey_get_details($res);
    	$pub_key = $arr['key'];
    	/*写入文件中*/
    	file_put_contents(ROOT.$this->private_path,$priv_key);
    	file_put_contents(ROOT.$this->pubilc_path,$pub_key);
    }
    /*获取公钥私钥*/
    private function getCerts()
    {
    	/*获取私钥*/
    	$priv_key = file_get_contents(ROOT.$this->priv_path);
		openssl_pkcs12_read($priv_key, $certs,'123456'); //读取公钥、私钥 
		return $certs;
    }
    /*私钥加密*/
    public function encrypt($data='')
    {
    	$certs = $this->getCerts();
    	$prikeyid = $certs['pkey']; //私钥
    	openssl_sign($data, $signMsg, $prikeyid); //注册生成加密信息  
		$signMsg = base64_encode($signMsg); //base64转码加密信息 
		return $signMsg;
    }
    /*公钥解密*/
    public function dencrypt($data='',$signMsg='')
    {
    	$unsignMsg=base64_decode($signMsg);//base64解码加密信息  
		/*获取私钥*/
    	$pubkeyid = openssl_x509_read(file_get_contents(ROOT.$this->pub_path));//公钥 
		$res = openssl_verify($data,$unsignMsg,$pubkeyid); //验证 
		var_dump($res);
    }
    /*私钥加密*/
    public function rasData($data='')
    {
    	$privateKey = openssl_pkey_get_private(file_get_contents(ROOT.$this->private_path),$this->key);
        ($privateKey) or die('密钥不可用');
        /*数据加密*/
        openssl_private_encrypt($data,$crypted,$privateKey);
        $crypted = base64_encode($crypted);
        return $crypted;
    }
    /*公钥解密*/
    public function dencryptRas($crypted='')
    {
    	$crypted = base64_decode($crypted);
    	$pubilcKey = openssl_pkey_get_public(file_get_contents(ROOT.$this->pubilc_path));
        ($pubilcKey) or die('密钥不可用');
        /*数据解密*/
        openssl_public_decrypt($crypted,$data,$pubilcKey);
        return $data;
    }
}
$openssl = new opensslClass();
$data = array(
        'tel'=>'肖雨',
        'pass'=>'123456'
    );
$signjson = json_encode($data);
//$sign = $openssl->encrypt($signjson);
$res = $openssl->rasData($signjson);
$data1 = json_decode($openssl->dencryptRas($res),true);
//print_r($res);
print_r($data1);
/*获取提交的方法*/
/*$sign = isset($_POST['sign']) ? trim($_POST['sign']) : '';
if(empty($sign))
{
    die(json_encode(array('code'=>'1','msg'=>'sign not')));
}*/
/*验证签名*/
/*$data = $_POST;
unset($data['sign']);
$jsonstr = json_encode($data);
$res = $openssl->dencrypt($jsonstr,$sign);
print_r($res);*/
?>